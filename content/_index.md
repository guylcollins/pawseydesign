---
layout: index
title: Drainage tests to BRE 365 and BS 6297
meta_page_description: Drainage tests to BRE 365 and BS 6297
onpagetitle: Drainage design for planning approval
menu:
  main:
    name: Home
    weight: 1

---
![Pawsey Design Ltd Drainage design for planning approval](https://res.cloudinary.com/amplifyme/image/upload/v1667821546/pawseydesign/Pawsey_Design_Ltd_acgemc.webp "Pawsey Design Ltd Drainage design for planning approval")

Increasingly the planning process imposes the need for clarity regarding both foul and surface water drainage. This can be either at the initial application stage or following planning approval when planning conditions with confusing requirements need to be discharged.

This is what we do. We work on simple house extensions or on small housing developments of say 3-4 houses as well as very large commercial units and agricultural buildings. We limit our projects to being within the South Hams in Devon but the guidance provided here will likely apply across most of England and Wales.

We can provide detailed drainage designs, detailing all of the drainage runs etc. but we generally work with you and your Architect where we provide simple schematic information for you or your Architect to develop for construction.

{{< greybox >}}

### RESERVATIONS

The construction environment is a very complex and ever changing one with two projects rarely being identical.

The advice given on this website and in the articles can only be of a general nature and will not necessarily be completely appropriate for your project. The guidance is intended to remove some of the mystery from the information required but you must use a suitably qualified and able professional to produce a project specific design.

Therefore, the guidance is given in good faith and cannot be taken as having any warranty either; expressed, inferred or otherwise.

Pawsey Design Ltd or Phil Pawsey will not accept any liability for any losses or damages resulting from the use of the guidance.

{{< /greybox >}}