---
layout: index
title: About
meta_page_description: Phil Pawsey has been providing; drainage designs, drawings, and specifications for construction since 2008
onpagetitle: About
menu: 
    main:
      weight: 2
      name: About
---

Phil Pawsey is the sole director of Pawsey Design Ltd. Phil has been providing; drainage designs, drawings, and specifications for construction since 2008 either taking projects from start to finish or working with Architects, Surveyors and Engineers as part of a team.

Drainage requirements for planning approvals can just look like a bewildering affair written in a foreign language. They can intimidate and frustrate normally calm people with terms like; Greenfield Runoff, tests to BRE 365, attenuated discharge, 1 in 100 year events, climate change, hierarchies, groundwater monitoring, SUDs etc. Phil Pawsey, from Pawsey Design Ltd, speaks the language so can interpret the mysteries of drainage. He can do the tests, come up with a solution and return your mental state to the tranquil ripple free state you enjoyed prior to the planning process.

Drainage designs can be for foul sewage and/or surface water runoff where the need for approval can be a deal breaker so early consideration is best.

Phil can work with your Architects giving just a schematic design to satisfy the approval process, which could include Devon County Council and South West Water as well as SHDC Development Management. This leaves control of the detailed design with the Architect.Alternatively Phil can design the pipework for Building Regulation approval and construction.