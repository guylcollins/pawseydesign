---
layout: gallery
title: Gallery
meta_page_description: Pawsey Design Image Gallery
onpagetitle: Gallery
gallery_item:
- image: "/v1667664207/pawseydesign/Shallow_surface_water_test_pit_for_a_track_liexoy.jpg"
  image_title: Shallow surface water test pit for a track
  image_description: ''
- image: "/v1667664183/pawseydesign/Tanker_to_flood_test_pit_gjxzot.jpg"
  image_title: Tanker to flood test pit
  image_description: ''
- image: "/v1667664070/pawseydesign/Deep_test_pit_for_a_house_soakway_svk2ot.jpg"
  image_title: Deep test pit for a house soakaway
  image_description: ''
- image: "/v1667664141/pawseydesign/IBC_s_ready_to_flood_a_test_pit_e2icol.jpg"
  image_title: IBC's ready to flood a test pit
  image_description: ''
- image: "/v1667664115/pawseydesign/Foul_water_test_pit_enzzfi.jpg"
  image_title: Foul water test pit
  image_description: ''
menu:
  main:
    weight: 4

---
