---
layout: index
title: Planning Glossary
meta_page_description: Glossary of Terms for Drainage tests to BRE 365 and BS 6297
onpagetitle: Planning Glossary
menu: 
    main:
      weight: 3
      name: Planning Glossary
---



## SURFACE WATER GLOSSARY:

The following gives a brief overview of each item, it does not go into exhaustive detail of the rules and implications of each item.

{{< glossary surfacewater >}}

### Surface Water:

This is run-off from roofs, hard standing areas etc. Basically, it is the clean rain that falls on your property and has to go somewhere.

### Attenuation:

When soakaways don’t work for some reason we sometimes store the rainwater in a tank or a pond and release a controlled flow from the tank to a combined sewer or watercourse. There are numerous ways of controlling the flow from the tank, see “Flow Control” below.

### Tests to BRE DG 365:

This is a booklet from the Building Research Establishment, (BRE), it is for surface water, not foul water. It describes how to do the tests to establish the infiltration rate of the ground and then it goes on to tell you how to design a soakaway by a long hand method.

### Building Regulations and Approved Document H:
In the past Building Control were in control of foul and surface water drainage on your property. They applied The Building Act 1984 and used Approved Document H as one way of proving compliance with the Act. Approved Document H is downloadable free of charge, you will find it by a simple Google search, just make sure you are working to the latest version.

The planning system has now moved in to provide control over the bigger picture, in particular with surface water to try and control flooding. They also, at times, include the foul discharge in the planning conditions.

In most cases when you achieve the planning approval you will then need Building Control approval and inspections as the works proceed.


### Climate Change:

Allowance is made for climate change, currently this is estimated be increasing rainfall by 40% in the coming years. This figure is just given to us and is not open to debate.

### Critical Drainage Area, CDA):

Just as you would imagine from the title, some places, probably in a valley, suffer more from flooding than others. In some places it doesn’t matter but, in the places where it really does matter the council identify the risk and impose additional, more onerous condition on the drainage designs. You can search on line for local CDA’s to see if you are in one. Even if you are not in one your run-off may feed into one so again the extra conditions can be applied.

### Flood Map for planning:
This shows what level of flood zone you are in and whether you are likely to be flooded. You can download this, just do a Google search “Flood Map for planning”. See Flood Risk Assessment, (FRA).

### Flood Risk Assessment, (FRA):
There are certain triggers, your flood zone, size of site etc. which determine whether you need an FRA. It is exactly what it sounds like, a risk assessment for flooding.

### Flow Control:
If we attenuate and discharge to a combined sewer or watercourse the rate of the discharge needs to be controlled. There are numerous ways of doing this but two often used types are an “Orifice” or “Vortex Flow Control”. The orifice is great because it is cheap, it is a hole in a bit of plate, or it can be a short length of pipe. The trouble is the head of water, (storage depth), is limited because if it is too great then the flow rate is too high and you don’t get approval. The vortex flow control, (Google “Hydrobrake”), is not limited by head of water but costs significantly more to buy and install.

### Greenfield Run-off rate:
If it rains on a field after a while the ground just can’t take it anymore. It throws the towel in and the water runs off. We work out the rate we believe it would run-off the field at. This is often referred to as the rate we can attenuate to for a discharge to a sewer or watercourse. It is so we don’t make it worse than the natural situation. This is great for large buildings or large sites but for a single house or extension we can’t achieve such a low figure for our discharge.

### Orifice:
See flow control above. The problem when designing with them is balancing the storage depth with the orifice, (hole), diameter and the available area in plan to give you the required outflow rate. If they are too small, they get blocked. 

### Percolation and Infiltration:
Same thing really, how quickly does water drain away through the soil at whatever level we build the soakaway. In the case of surface water we do the tests to BRE DG 365 to find the infiltration rate then work out the soakaway size if it will work.

### Rainfall 1 in 100 year event:
This does not mean a rainfall event that you will have once in a 100 years. It is an event that you will possibly have 1 out of every 100 rain storms. 

### Riperian Owner:
If you want to discharge to a watercourse you must be sure you have the right to do so. You must be the riparian owner or have their permission.

### Soakaway:
A hole in the ground you discharge your roof water into. Common ones have stone or plastic crates, (they look a bit like milk crates), in the bottom to provide storage and stop the sides of the hole falling in. There are other ways of forming them. You cannot discharge the outflow from a foul water septic tank or treatment plant to a surface water soakway.

### Sustainable Urban Drainage Systems, SuDS, CIRIA C753:
This is the industry standard document for surface water drainage design. It is very big and can be downloaded free of charge from the CIRIA website.

### Water and Sewage Company:
This is the company you pay your water rates to. In most cases you have to have their permission to change an existing or make a new foul or surface water discharge to their system.


{{< /glossary >}}


## FOUL WATER GLOSSARY:
The following gives a brief overview of each item, it does not go into exhaustive detail of the rules and implications of each item.

{{< glossary foulwater >}}

### Foul Water:
From flushing the toilet and emptying sinks, washbasins, washing machines and numerous other things.

### Building Regulations and Approved Document H:
In the past Building Control were in control of foul and surface water drainage on your property. They applied The Building Act 1984 and used Approved Document H as one way of proving compliance with the Act. Approved Document H is downloadable free of charge, you will find it by a simple Google search, just make sure you are working to the latest version.

The planning system has now moved in to provide control over the bigger picture, in particular with surface water to try and control flooding. They also, at times, include the foul discharge in the planning conditions.

In most cases when you achieve the planning approval you will then need Building Control approval and inspections as the works proceed.

### Cess Pit or Cess Pool:
This is a closed system without a discharge. It is a tank which is emptied frequently. Many authorities will not accept them anymore.

### Drainage Field:
This is where you drain a septic tank to and a treatment plant if not discharging it to a watercourse. The tests for a drainage field design are included in Approved Document H, they are different to a surface water soakaway test.

### Environment Agency, (E.A.), Consent:
If you are installing a Septic Tank or Sewage Treatment Plant you may need a permit from the E. A. for the discharge. First of all, check the General Binding Rules below.

### Form FDA1:
This is a planning form to show how you intend to deal with your foul sewage and wastes.

### General Binding Rules, (GBR):
This is an important set of rules to check whether you need to apply for a permit for the discharge of the outflow from your treatment plant or septic tank. You can find it by a Google Search but also check out the later addition which clarifies some of the searches you must do.

### Riperian Owner:
If you want to discharge to a watercourse you must be sure you have the right to do so. You must be the riparian owner or have their permission.

### Septic Tank:
This was the most common type of private sewage treatment but is being replaced with the sewage treatment plant. The septic tank must discharge to ground via a drainage field. If everything is suitable they can still be specified but they are becoming increasingly unpopular in favour of treatment plants. Under no circumstances can you discharge from a septic tank direct to a watercourse, it is illegal. They cannot discharge to a surface water soakaway.

### Sewage Treatment Plant:
These do the job of the septic tank but better. Most need an electrical supply and the ones that don’t may not be the best solution. They can discharge to a drainage field or direct to a watercourse if the GMR apply or you have a permit from the E.A. They cannot discharge to a surface water soakaway.

### Water and Sewage Company:
This is the company you pay your water rates to. In most cases you have to have their permission to change an existing or make a new foul or surface water discharge to their system.

{{< /glossary >}}